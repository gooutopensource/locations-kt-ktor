FROM openjdk:11-jre-slim

ENV APPLICATION_USER ktor
RUN adduser --disabled-login --gecos '' $APPLICATION_USER

RUN mkdir /app
RUN chown -R $APPLICATION_USER /app

USER $APPLICATION_USER

COPY ./build/libs/locations-ktor-0.0.1-all.jar /app/app.jar
WORKDIR /app

CMD ["java", "-XX:MaxRAMPercentage=75", "-jar", "app.jar"]
