package com.goout.config

import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.features.ParameterConversionException
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond


fun StatusPages.Configuration.handleErrors() {
    respondWithMessage<RequiredParameterIsNullException>(HttpStatusCode.BadRequest)
    respondWithMessage<ParameterConversionException>(HttpStatusCode.BadRequest)
    respondWithMessage<NotFoundException>(HttpStatusCode.NotFound)
    exception<Throwable> {
        log.error("Error when serving request ${call.request}", it)
        call.respond(
                HttpStatusCode.InternalServerError,
                ErrorResponse()
        )
    }
}


inline fun <reified T: Throwable>StatusPages.Configuration.respondWithMessage(statusCode: HttpStatusCode) {
    exception<T> {
        call.respond(statusCode, ErrorResponse(it.message))
    }
}


class RequiredParameterIsNullException(name: String): Throwable("Parameter $name is null")


data class ErrorResponse(
        val message: String? = null
)
