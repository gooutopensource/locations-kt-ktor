package com.goout.config

import com.goout.controller.cityController
import com.goout.service.CityService
import io.ktor.routing.Routing

fun Routing.api() {
    // instantiate services
    val cityService = CityService()

    // register controller(s)
    cityController(cityService)
}
