package com.goout.service

import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.databind.ObjectMapper
import com.goout.controller.CityResponse
import com.goout.controller.Language
import io.inbot.eskotlinwrapper.AsyncIndexRepository
import io.inbot.eskotlinwrapper.JacksonModelReaderAndWriter
import io.ktor.features.NotFoundException
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.asyncIndexRepository
import org.elasticsearch.search.fetch.subphase.FetchSourceContext
import java.util.concurrent.ConcurrentHashMap


class CityService {
    companion object {
        private inline fun <reified T : Any> RestHighLevelClient.repository(index: String): AsyncIndexRepository<T> =
            asyncIndexRepository(
                index,
                fetchSourceContext = FetchSourceContext(true, null, arrayOf("centroid", "geometry")),
                // We must override the default because it uses SNAKE_CASE PropertyNamingStrategy.
                modelReaderAndWriter = JacksonModelReaderAndWriter.create(ObjectMapper().findAndRegisterModules())
            )
    }

    private val cityRepository = ElasticCientFactory.client.repository<ElasticCity>("city")

    private val regionRepository = ElasticCientFactory.client.repository<ElasticRegion>("region")

    private val regionCache = ConcurrentHashMap<Long, ElasticRegion>()


    suspend fun getCity(id: Long, language: Language): CityResponse {
        val city = cityRepository.get("$id") ?: throw NotFoundException("City#${id} does not exists.")
        val region = getRegion(city.regionId) ?: throw RuntimeException("City#${city.id} links non existent Region#${city.regionId}")
        return CityResponse(
                id = city.id,
                isFeatured = city.isFeatured ?: false,
                countryIso = city.countryIso,
                name = city.names[language.nameKey()] ?: throw RuntimeException("City#${city.id} is missing name in Language[${language}]"),
                regionName = region.names[language.nameKey()] ?: throw RuntimeException("Region#${region.id} is missing name in Language[${language}]")
        )
    }

    private suspend fun getRegion(id: Long): ElasticRegion? {
        var region = regionCache[id]
        if (region != null) {
            return region
        }

        region = regionRepository.get("$id")
        if (region != null) {
            regionCache[id] = region
        }
        return region
    }

    private fun Language.nameKey() = "name.${name.toLowerCase()}"
}


data class ElasticCity (
        val id: Long,
        val regionId: Long,
        val countryIso: String,
        val isFeatured: Boolean?
) {
    // For now Jackson does not support this for constructor parameters https://github.com/FasterXML/jackson-databind/issues/562
    @JsonAnySetter
    val names: MutableMap<String, String> = mutableMapOf()
}


data class ElasticRegion (
        val id: Long,
        val countryIso: String
        ) {
    // For now Jackson does not support this for constructor parameters https://github.com/FasterXML/jackson-databind/issues/562
    @JsonAnySetter
    val names: MutableMap<String, String> = mutableMapOf()
}
