package com.goout.service

import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.create

object ElasticCientFactory {
    val client: RestHighLevelClient
    private var elastic_host = System.getenv("GOOUT_ELASTIC_HOST")
    private var elastic_port = System.getenv("GOOUT_ELASTIC_PORT")


    init {
        client = create(elastic_host, elastic_port.toInt())
        // TODO make this asynchronous
        client.ping(RequestOptions.DEFAULT)
    }
}